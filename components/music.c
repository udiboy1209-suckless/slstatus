#include <linux/socket.h>
#include <mpd/client.h>
#include <stdio.h>
#include <string.h>

#include "../util.h"

char mpdhost[256] = "127.0.0.1";
unsigned int mpdport = 6600;

static struct mpd_connection* mpdc = NULL;

const char*
mpd_status(void)
{
    if(mpdc == NULL) {
        if((mpdc = mpd_connection_new(mpdhost, mpdport, 0)) == NULL) {
            fprintf(stderr, "Connection to mpd %s:%d failed", mpdhost, mpdport);
            return NULL;
        }
    }
    if(mpd_connection_get_error(mpdc) != MPD_ERROR_SUCCESS) {
        fprintf(stderr, "Connection to mpd %s:%d error", mpdhost, mpdport);
        mpd_connection_free(mpdc);
        mpdc = NULL;
        return NULL;
    }

    char song_title[40] = "~";
    char song_artist[40] = "~";
    char song_album[40] = "~";
    const char *r = NULL;
    enum mpd_state st;
    struct mpd_status *s = NULL;
    struct mpd_song *so = NULL;
    int op = 0;

    if((s = mpd_run_status(mpdc)) == NULL) {
        return NULL;
    }
    st = mpd_status_get_state(s);
    if(st == MPD_STATE_STOP || st == MPD_STATE_UNKNOWN)
        goto cleanup;
    if((so = mpd_run_current_song(mpdc)) == NULL)
        goto cleanup;

    if((r = mpd_song_get_tag((const struct mpd_song*)so,
                    MPD_TAG_TITLE, 0)) != NULL)
        strcpy(song_title, r);
    if((r = mpd_song_get_tag((const struct mpd_song*)so,
                    MPD_TAG_ARTIST, 0)) != NULL)
        strcpy(song_artist, r);
    if((r = mpd_song_get_tag((const struct mpd_song*)so,
                    MPD_TAG_ALBUM, 0)) != NULL)
        strcpy(song_album, r);
    op = 1;

cleanup:
    mpd_status_free(s);
    if(so != NULL)
        mpd_song_free(so);
    if(op)
        return bprintf("%s - %s - %s", song_artist, song_album, song_title);
    return NULL;
}
