#define _GNU_SOURCE     /* To get defns of NI_MAXSERV and NI_MAXHOST */
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <stdio.h>
#include <unistd.h>
#include <linux/if_link.h>

#include "../util.h"

#define MB (1024*1024)
#define KB 1024

int rx_prev = 0, tx_prev = 0;

const char *
netspeed()
{
    struct ifaddrs *ifaddr, *ifa;
    int family, n, rx = 0, tx = 0, sp, d;

    if (getifaddrs(&ifaddr) == -1) {
        fprintf(stderr, "getifaddrs failed");
        return NULL;
    }

    /* Walk through linked list, maintaining head pointer so we
       can free list later */

    for (ifa = ifaddr, n = 0; ifa != NULL; ifa = ifa->ifa_next, n++) {
        if (ifa->ifa_addr == NULL)
            continue;

        family = ifa->ifa_addr->sa_family;

        /* Sum up the speed */
        if (family == AF_PACKET && ifa->ifa_data != NULL) {
            struct rtnl_link_stats *stats = ifa->ifa_data;
            rx += stats->rx_bytes;
            tx += stats->tx_bytes;
        }
    }

    freeifaddrs(ifaddr);

    sp = (tx - tx_prev > rx - rx_prev ? tx - tx_prev : rx - rx_prev);
    d = tx - tx_prev > rx - rx_prev ? 1 : 0;
    rx_prev = rx; tx_prev = tx;

    if (sp > MB)
        return bprintf("%2.2fM%s", (float)sp / MB, d ? "" : "");
    else if (sp > KB)
        return bprintf("%4dK%s", sp / KB, d ?"" : "");
    else 
        return bprintf("%4dB%s", sp, d ?"" : "");
}
